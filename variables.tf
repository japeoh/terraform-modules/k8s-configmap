variable domain {}
variable unit {}
variable environment {}
variable project {}
variable k8s_namespace {}
variable cm_name {}
variable cm_data {
  type = map(string)
  default = {}
}
variable cm_binary_data {
  type = map(string)
  default = {}
}
