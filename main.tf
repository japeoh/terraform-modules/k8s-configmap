resource kubernetes_config_map cm {
  metadata {
    name      = var.cm_name
    namespace = var.k8s_namespace
    labels    = local.labels
  }

  data        = var.cm_data
  binary_data = var.cm_binary_data
}
